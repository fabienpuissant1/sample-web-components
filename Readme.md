# Building a Design System of Web Components on GitLab

## Overview

In a context where many front-end technologies such as React, Vue, Angular, or even Vanilla are used to build projects, it can be challenging for a company using multiple of them to establish a consistent and shareable Design system across all its projects. To solve this problem, we can utilize web components, which return to basics by creating HTML components that can be utilized by any front-end technology, thereby allowing the frameworks to handle complex JavaScript logic between components.

Here, we will explore how to build web components with Stencil ([Stencil](https://stenciljs.com/)), how to test them, integrate Storybook ([Storybook](https://storybook.js.org/)) for comprehensive documentation sharing, and finally, leverage GitLab to automate build, testing, npm library creation, and deploy our Storybook on GitLab Pages.

## Technologies

- Stencil: a web component framework
- Lerna: a monorepo manager to produce components for various front-end frameworks and manage versioning
- Storybook: for creating documentation
- GitLab: for hosting code, Docker images, npm libraries, CI/CD, and deploying a static website for Storybook

## Why Monorepo?

We utilize Lerna as a monorepo tool because even though components are written once and used everywhere, we need to create output targets. Stencil will use the components developed under the package sample-web-components and generate other packages for specific integrations, such as React, with minimal configuration ([Stencil Documentation](https://stenciljs.com/docs/overview)). Additionally, Lerna ensures consistent versioning across linked packages.

# Development Stage

## Developing Web Components

The web components you create will reside under the sample-web-components package. In **src/components**, you'll find an example of a button. For now, don't focus on the **stories** folder. Here, you can see the tsx file containing the web components, scss file containing styles, and the test folder where we have unit and e2e tests. Refer to the Stencil documentation for building your own components.

To preview the web components' result, start the Stencil web server by using:

```
lerna run start
```

Open **http://localhost:3333**.

Then, in the **src/index.html**, you have a playground to check the styles and test the props JavaScript logic.

## Ensuring Tests Pass

To test the components, you can use:

```
lerna run test
```

Or, if you're in development, you can also use watch mode:

```
lerna run test.watch
```

## Storybook

Next, we add documentation to the web components. Open the **stories** folder in the web components folder. Here, you'll find the elements that Storybook needs to create the static website. Refer to the Storybook documentation to learn how to write stories ([Storybook Documentation](https://storybook.js.org/docs/writing-stories)).

To see the result of the static website, ensure you've built the web components with:

```
lerna run build
```

Then, start the Storybook server:

```
lerna run storybook
```

Open **http://localhost:6006**.

## Versioning

Once you've created, tested, and documented your components, you're ready to create your first version. We'll use Lerna to manage the versioning, which will bump all the packages. Ensure you don't have uncommitted changes and type:

```
lerna version
```

This will prompt you to choose a version to bump to.

# DevOps Stage

## GitLab Registry Setup

Firstly, configure the GitLab Container Registry.

We'll use two custom images, one for the build and one for the tests. Both Dockerfiles are in the repository.

For the build, pick a node version and install Lerna to avoid installing it in all our stages:

```
docker build -t ${YOUR_REGISTRY_ADDRESS}/node-lerna .
docker push ${YOUR_REGISTRY_ADDRESS}/node-lerna .
```

For the tests, configure the image to run tests into GitLab CI without Puppeteer errors:

```
docker build -t ${YOUR_REGISTRY_ADDRESS}/lerna-puppeteer -f Dockerfile-test .
docker push ${YOUR_REGISTRY_ADDRESS}/lerna-puppeteer .
```

Now, adapt the image variables at the top of the .gitlab-ci.yml file using your own image URLs.

## Build and Test Stage

The first stages are build and tests, triggered every time you push your code. These stages will build the Stencil components, build the Storybook, and run all tests. Ensure to adapt this depending on your requirements.

## Deployment

The deploy stage will only trigger when you create a tag. I recommend naming your tag corresponding to the version that you will deploy with Lerna.

In this stage, you have two jobs. The first is the GitLab Pages job, which will deploy the static website on GitLab Pages based on the Storybook build. You can find it in GitLab Deploy > Pages, then you have the URL where the Storybook is served.

The second job will publish the npm library. As you understood, you will have as many npm libraries as many output targets you created. You can find them in Deploy > Package Registry. If you want to change the name of the libraries, ensure to respect the GitLab conventions ([GitLab Documentation](https://docs.gitlab.com/ee/user/packages/npm_registry/)) and adapt the deploy stage in the gitlab-ci.yml.

For example, in a real case, @sample was, in fact, the name of my organization/project: @my-organization-name.

# Share the Libraries

Now, we are ready to share these components with all projects within my organization. In this section, we will learn how to install the library into an npm project, such as a React App in my case.

Based on the group, follow the GitLab tutorial ([GitLab Documentation](https://docs.gitlab.com/ee/user/packages/npm_registry/)). For me, as it is at the project level, and my project ID is 56597885, I just have to configure npm with:

```
npm config set @sample:registry=https://gitlab.com/api/v4/projects/56597885/packages/npm/
```

Of course, in a real case, make sure to authenticate to the registry with an authentication token.

Then, you can use it as a component, for example, in my React App:

```javascript
import { SampleButton } from "@sample/react-web-components";
```

# Next Steps

The first and important next step is to establish a versioning system for the Storybook documentation.

Another improvement I have in mind is to automate the version bumping in Lerna when we tag. Currently, we have to bump versions with Lerna locally and then create a tag to trigger the publish onto the npm registry. It would be more convenient to make GitLab handle this process in CI so that it will automatically bump and deploy based on the git tag name.
