export const createElement = ({ label, primary, disabled, rounded, width, height, fontSize, click }) => {
  const btn = document.createElement('sample-button');
  btn.setAttribute('label', label);
  btn.setAttribute('rounded', rounded);
  btn.setAttribute('primary', `${primary}`);
  btn.setAttribute('disabled', `${disabled}`);
  btn.setAttribute('height', height);
  btn.setAttribute('width', width);
  btn.setAttribute('font-size', fontSize);
  btn.addEventListener('click', click);
  return btn;
};
