import { newE2EPage } from '@stencil/core/testing';

describe('sample-button', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button></sample-button>');
    const element = await page.find('sample-button');
    expect(element).toHaveClass('hydrated');
    const button = await page.find('sample-button >>> button');
    expect(button).toHaveClass(`button`);
  });

  it('renders with label', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button label="firstLabel"></sample-button>');
    const component = await page.find('sample-button');
    const element = await page.find('sample-button >>> button');
    expect(element.textContent).toEqual(`firstLabel`);

    component.setProperty('label', 'secondLabel');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`secondLabel`);
  });

  it('renders with primary props', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button primary></sample-button>');
    const element = await page.find('sample-button >>> button');
    expect(element.className).toContain(`primary`);
  });

  it('renders with primary props per default', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button></sample-button>');
    const element = await page.find('sample-button >>> button');
    expect(element.className).toContain(`primary`);
  });

  it('renders with rounded props', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button rounded="medium"></sample-button>');
    const element = await page.find('sample-button >>> button');
    expect(element.className).toContain(`rounded-medium`);
  });

  it('renders with width props', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button width="100px"></sample-button>');
    const element = await page.find('sample-button >>> button');
    expect((await element.getComputedStyle()).width).toEqual('100px');
  });

  it('renders with height props', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button height="100px"></sample-button>');
    const element = await page.find('sample-button >>> button');
    expect((await element.getComputedStyle()).height).toEqual('100px');
  });

  it('renders with font-size props', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button font-size="10px"></sample-button>');
    const element = await page.find('sample-button >>> button');
    expect((await element.getComputedStyle()).fontSize).toEqual('10px');
  });

  it('renders with disabled props', async () => {
    const page = await newE2EPage();

    await page.setContent('<sample-button disabled></sample-button>');
    const element = await page.find('sample-button >>> button');
    expect(element).toHaveAttribute('disabled');
  });
});
