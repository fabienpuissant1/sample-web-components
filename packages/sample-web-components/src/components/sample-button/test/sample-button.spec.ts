import { newSpecPage } from '@stencil/core/testing';
import { SampleButton } from '../sample-button';

describe('sample-component', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [SampleButton],
      html: '<sample-button></sample-button>',
    });
    expect(root).toEqualHtml(`
        <sample-button primary>
          <mock:shadow-root>
          <button class="button primary"></button>
          </mock:shadow-root>
        </sample-button>
      `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [SampleButton],
      html: `<sample-button label="Stencil" primary rounded="medium" width="100px" height="200px" disabled font-size="10px"></sample-button>`,
    });
    expect(root).toEqualHtml(`
        <sample-button label="Stencil" primary rounded="medium" width="100px" height="200px" disabled font-size="10px">
          <mock:shadow-root>
            <button class="button primary rounded-medium disabled" style="width: 100px; height: 200px; font-size: 10px;" disabled>Stencil</button>
          </mock:shadow-root>
        </sample-button>
      `);
  });
});
